import $ from 'jquery';

const body = $('body');

export default {
  updateActivePanel(state, payload) {
    $('.not-index, [data-index]').removeClass('active');
    if (!payload) {
      state.paymatSuccsesText = null;
      state.slidePanel.active = null;
      return;
    }
    if (typeof payload === 'string') {
      state.slidePanel.active = payload;
      return;
    }
    state.slidePanel.active = payload.active;
    state.slidePanel.direction = payload.direction;
  },
  setDashboardPage(state, payload) {
    state.dashboardPage = {
      data: payload.data,
      filterCurr: payload.filterCurr,
      defaultCurr: payload.filterCurr,
    };
  },
  setFilterDashbord(state, payload) {
    state.dashboardPage.filterCurr = payload;
  },
  setDefaultFilterDashbord(state, payload) {
    state.dashboardPage.defaultFilter = payload;
  },
  updateDataPanel(state, payload) {
    state.slidePanel.data = payload;
  },
  closeAllModal(state) {
    state.slidePanel.active = null;
    state.activePanelPaymant = false;
    body.removeClass('no-scroll');
  },
  updateCryptoCurrency(state, payload) {
    state.cryptoCurrency = payload;
  },
  setWindowSize(state, payload) {
    state.windowSize = payload;
  },
  setCurrencyHistory(state, payload) {
    state.currencyHistory = payload;
  },
  setTransactionDetails(state, payload) {
    state.transactionDetails = { ...state.transactionDetails, ...payload };
  },
  setFilterHistory(state, payload) {
    state.filterHistory = payload;
  },
  setPanelPaymant(state, payload) {
    body.addClass('no-scroll');
    state.activePanelPaymant = payload;
    if (!payload) {
      body.removeClass('no-scroll');
      state.errorPaymant = null;
      state.paymatSuccsesText = null;
    }
  },
  setFormGetPin(state, payload) {
    state.formGetPin = { ...state.formGetPin, ...payload };
  },
  setErrorPaymant(state, payload = null) {
    state.errorPaymant = payload;
  },
  setColdStorage(state, payload) {
    state.coldStorageData = payload;
  },
  setWithdrawAmount(state, payload) {
    state.withdrawAmount = payload;
  },
  setColdStorageCurrCode(state, payload) {
    state.coldStorageCurrCode = payload;
  },
  setExchangeData(state, payload) {
    state.exchangeData = payload;
  },
  setExchangeInfoOperation(state, payload) {
    state.exchangeInfoOperation = payload;
  },
  setActiveScaner(state, payload) {
    state.activeScaner = payload;
  },
  setSavingAccData(state, payload) {
    state.savingAccData = payload;
  },
  setLoanData(state, payload) {
    state.loanData = payload;
  },
  setPaymatSuccsesText(state, payload) {
    state.paymatSuccsesText = payload;
  },
  setDataInterestRatea(state, payload) {
    state.dataInterestRate = payload;
  },
  setLoanSingleData(state, payload) {
    state.loanSingleData = payload;
  },
};
