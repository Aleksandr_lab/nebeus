import { formatDataSavingAcc, formatDataLoan } from '@/services';
import numberFormat from '@/helpers/numberFormat';

export default {
  formatAmount: ({ cryptoCurrency }) => (num = null, isCrypto = true) => {
    if (cryptoCurrency) {
      // eslint-disable-next-line no-param-reassign
      isCrypto = cryptoCurrency.isCrypto;
    }
    return numberFormat(num || num === 0 ? num : cryptoCurrency.currCurrency.Amount, isCrypto ? 6 : 2);
  },
  formatAmountUp: ({ slidePanel: { data } }) => (num, name) => {
    if (!data) return null;
    const arrCrypto = [...Object.keys(data.currencies.crypto), ...Object.keys(data.currencies.token)];
    const isCrypto = arrCrypto.includes(name);
    return numberFormat(num, isCrypto ? 6 : 2);
  },
  getSevaAccDeposit: ({ savingAccData }) => (status) => {
    if (!savingAccData || !status) {
      return [];
    }
    return savingAccData.infoTable
      .filter(({ DepositStatus }) => DepositStatus === status)
      .sort((a, b) => (new Date(a.CreationDate) > new Date(b.CreationDate) ? -1 : 0))
      .map((item) => {
        const currTemplate = savingAccData.templates.find(({ Id }) => Id === item.DepositTemplateId);
        if (!currTemplate) {
          return null;
        }
        return { paymant: currTemplate.PercentAnnual, ...formatDataSavingAcc(item) };
      });
  },
  getLoanDate: ({ loanData }) => (status) => {
    if (!loanData || !status) {
      return [];
    }
    return loanData.infoTable
      .filter(({ Status }) => Status === status)
      .sort((a, b) => (new Date(a.CreationDate) > new Date(b.CreationDate) ? -1 : 0))
      .map((item) => {
        const currTemplate = loanData.templates.find(({ Id }) => Id === item.TemplateId);
        if (!currTemplate) {
          return formatDataLoan(item);
        }
        return { template: currTemplate, ...formatDataLoan(item) };
      });
  },
  isCrypto: ({ slidePanel: { data } }) => (name) => {
    if (!data) return null;
    const arrCrypto = [...Object.keys(data.currencies.crypto), ...Object.keys(data.currencies.token)];
    return arrCrypto.includes(name);
  },
  getColdCurrentData: ({ coldStorageCurrCode, coldStorageData, cryptoCurrency }) => {
    if (cryptoCurrency) {
      const { Amount, AmountInDefaultCurrency, Id } = cryptoCurrency.currCurrency;
      return {
        SrcSubBalanceId: Id,
        amount: Amount,
        amountInDefaultCurrency: AmountInDefaultCurrency,
      };
    }
    if (!coldStorageCurrCode || !coldStorageData) return null;
    return coldStorageData.data[coldStorageCurrCode];
  },
  getLimitForms: ({ slidePanel, cryptoCurrency }) => (name, max = false, code) => {
    if (!slidePanel.data) return '';
    const { optionsDashboard, userVerifies } = slidePanel.data;
    if (max) {
      const isVerify = Object.values(userVerifies).filter((val) => val !== 'verified');
      if (!isVerify.length) return null;
    }
    let currCode = cryptoCurrency ? cryptoCurrency.currCurrency.CurrencyCode.toLowerCase() : '';
    if (code) {
      currCode = code.toLowerCase();
    }
    return Number(optionsDashboard[name][`${currCode}_${max ? 'max' : 'min'}`]);
  },
};
