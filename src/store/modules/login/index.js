export default {
  state: {
    countries: [],
  },
  mutations: {
    updateCountries(state, payload) {
      state.countries = payload;
    },
  },
};
