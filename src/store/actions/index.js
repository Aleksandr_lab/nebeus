import $ from 'jquery';
import errors from '../../helpers/errors';

export default {
  getCurrensyHistory({ commit, state }, defaultData) {
    commit('setFilterHistory', 'load');
    const data = {
      ...{
        currCode: state.cryptoCurrency ? state.cryptoCurrency.currCurrency.CurrencyCode : '',
        nebeusAction: 'getHistoryTransactions',
        FromDt: '',
        ToDt: '',
        PageNumber: 0,
        Filters: [],
        PageSize: 150,
      },
      ...defaultData,
    };
    $.ajax({
      url: state.ajaxUrl,
      method: 'POST',
      data,
      success(res) {
        if (Array.isArray(res)) {
          if (res[0] && res[0].Id) {
            const history = res.map((el) => ({
              ...el,
              ...{
                OperationAmount: el.Amount,
                OperationDate: el.CreationDate,
                status: el.StatusDescription,
              },
            }));
            commit('setCurrencyHistory', history);
            commit('setFilterHistory', history);
            return;
          }
          commit('setCurrencyHistory', res);
          commit('setFilterHistory', res);
        } else {
          commit('setFilterHistory', 'error');
        }
      },
      error() {
        commit('setFilterHistory', 'error');
      },
    });
  },
  postPaymanGetPin({ commit, state }, data) {
    if (state.formGetPin.load) return;
    commit('setErrorPaymant', false);
    commit('setFormGetPin', { load: true });
    $.ajax({
      url: state.ajaxUrl,
      method: 'POST',
      data,
      // eslint-disable-next-line camelcase
      success({ status, code_id, message }) {
        // eslint-disable-next-line camelcase
        if (!status || status !== 'success' || !code_id) {
          commit('setFormGetPin', { res: null });
          commit('setErrorPaymant', (message || errors.errorSend));
          return;
        }
        commit('setErrorPaymant', false);
        commit('setFormGetPin', { res: code_id });
      },
      error() {
        commit('setErrorPaymant', errors.errorSend);
      },
      complete() {
        commit('setFormGetPin', { load: false });
      },
    });
  },
  getItemDetails({ commit, state }, { Hash, OperationAmount, CurrencyCode }) {
    commit('setTransactionDetails', { load: true, data: null, error: null });
    commit('updateActivePanel', 'transactionDetails');
    $.ajax({
      url: state.apiUrlHistory + Hash,
      success(res) {
        if (!res) {
          commit('setTransactionDetails', { error: errors.errorSend });
          return;
        }
        const activeOutputs = res.outputs.find(({ value }) => value === (Math.abs(OperationAmount) * 1e8));

        const formatingRes = {
          OperationAmount,
          CurrencyCode,
          hash: res.hash,
          confirmations: res.confirmations,
          payerAddress: activeOutputs ? activeOutputs.script : res.outputs[0].script,
        };

        commit('setTransactionDetails', { data: formatingRes });
      },
      error() {
        commit('setTransactionDetails', { error: errors.errorSend });
      },
      complete() {
        commit('setTransactionDetails', { load: false });
      },
    });
  },
};
