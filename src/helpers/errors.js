export default {
  errorSend: 'Sorry, something went wrong. Please try again later.',
  minLengthPhone: 'Phone number must be at least 11 characters and not more than 13',
  minLengthPinCode: 'Pin code must be at least 4 characters',
  pinInvalid: 'PIN is invalid',
  invalidCard: 'Invalid Credit Card Number.',
  invalidCardData: 'Invalid Expiration Date.',
  disabledCard: 'Sorry, but we do not accept this type of card.',
  notEnoughFunds: 'Not enough funds on the wallet to complete the operation.',
  scanner: {
    notAllowedError: 'ERROR: you need to grant camera access permisson',
    notFoundError: 'ERROR: no camera on this device',
    notSupportedError: 'ERROR: secure context required (HTTPS, localhost)',
    notReadableError: 'ERROR: is the camera already in use?',
    overconstrainedError: 'ERROR: installed cameras are not suitable',
    streamApiNotSupportedError: 'ERROR: Stream API is not supported in this browser',
  },
};
