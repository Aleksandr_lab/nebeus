export default (number, decimals = 0, decЗoint = '.', thousandsSep = ',') => {
  const sign = number < 0 ? '-' : '';

  if (number === 0) {
    return Number().toFixed(decimals);
  }
  // eslint-disable-next-line
  const sNumber = `${Math.abs(parseInt(number = (Number(number) || 0).toFixed(decimals)))}`;
  const len = sNumber.length;
  const tchunk = len > 3 ? len % 3 : 0;

  const chFirst = (tchunk ? sNumber.substr(0, tchunk) + thousandsSep : '');
  const chRest = sNumber.substr(tchunk)
    .replace(/(\d\d\d)(?=\d)/g, `$1${thousandsSep}`);
  const chLast = decimals
    ? decЗoint + (Math.abs(number) - sNumber)
      .toFixed(decimals)
      .slice(2)
    : '';
  const format = sign + chFirst + chRest + chLast;
  return Number(format) === 0 ? '' : format;
};
