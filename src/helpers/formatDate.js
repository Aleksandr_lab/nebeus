import moment from 'moment';

export default (data, format = 'DD.MM.YYYY') => moment(data).format(format);
