const formatDataSavingAcc = (item) => ({
  id: item.Id,
  status: item.DepositStatus,
  dateCreate: item.CreationDate,
  dateClose: item.EndDate,
  balance: item.Amount,
  currCode: item.CurrencyCode,
  rateToEuro: item.RateToEuro,
});

const formatDataLoan = (item) => ({
  id: item.Id,
  status: item.State,
  dateCreate: item.CreationDate,
  dateClose: item.EndDate,
  paymant: item.NextPaymentInPledgeCurrency,
  balance: item.LoanAmount,
  monthlyPayment: item.MonthlyPayment,
  currCode: item.LoanCurrencyCode,
  guarantyAmount: item.GuarantyAmount,
  guarantyCurrencyCode: item.GuarantyCurrencyCode,
});

export {
  formatDataSavingAcc,
  formatDataLoan,
};
