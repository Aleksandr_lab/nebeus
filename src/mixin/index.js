export default {
  methods: {
    formLoad() {
      const btn = this.$el.querySelector('[type="submit"]');
      btn.classList.add('loaded');
      btn.disabled = true;
    },
    copyText(el) {
      const range = document.createRange();
      range.selectNode(el);
      window.getSelection().removeAllRanges();
      window.getSelection().addRange(range);
      document.execCommand('copy');
    },
    wordSlice(text, word = 20, clamp = '...') {
      const replaceText = text.replace(/\s+/g, ' ');
      const sliceText = replaceText.split(' ').slice(0, word).join(' ');
      if (sliceText.length === text.length) {
        return sliceText;
      }
      return `${sliceText} ${clamp}`;
    },
    truncate(text, length = 15, clamp = '...') {
      const replaceText = text.replace(/\s+/g, ' ').trim();
      const sliceText = replaceText.slice(0, length);
      if (sliceText.length === text.length) {
        return sliceText;
      }
      return `${sliceText} ${clamp}`;
    },
    formaterClass(str) {
      return str.replace(/\s+/g, '-').toLowerCase();
    },
  },
};
