import './assets/css/app.scss';
import $ from 'jquery';
import Vue from 'vue';
import './directive';
import './registerServiceWorker';
import VueTheMask from 'vue-the-mask';
import VCalendar from 'v-calendar';
import VueCardFormat from 'vue-credit-card-validation';
import Vue2TouchEvents from 'vue2-touch-events';
import router from './router';
import store from './store';
import i18n from './lang';
import BtnToggleSlidePanel from './components/buttons/BtnToggleSlidePanel.vue';

Vue.use(Vue2TouchEvents);
Vue.use(VueCardFormat);
Vue.use(VueTheMask);
Vue.use(VCalendar);


Vue.config.productionTip = true;

Vue.component('BtnToggleSlidePanel', BtnToggleSlidePanel);
Vue.component('TollbarPagas', () => import(/* webpackChunkName: "tollbar-pagas" */ './components/toollbar-page-dashbods/TollbarPagas.vue'));
Vue.component('login', () => import(/* webpackChunkName: "login" */ './components/login/Login.vue'));
Vue.component('NavSwiper', () => import(/* webpackChunkName: "nav-swiper" */ './components/navigation/NavSwiper.vue'));
Vue.component('SlidePanelLeft', () => import(/* webpackChunkName: "slide-panel-left" */ './components/slide-panel/SlidePanelLeft.vue'));
Vue.component('SlidePanelRight', () => import(/* webpackChunkName: "slide-panel-right" */ './components/slide-panel/SlidePanelRight.vue'));
Vue.component('DashboardDateTable', () => import(/* webpackChunkName: "dashboard-date-table" */ './components/dashbord-data-table/DashboardDateTable.vue'));
Vue.component('CryptoCurrencyPage', () => import(/* webpackChunkName: "crypto-currency-page" */ './components/crypto-currency/CryptoCurrencyPage.vue'));
Vue.component('ColdStoragePage', () => import(/* webpackChunkName: "cold-storage-page" */ './components/cold-storage/ColdStoragePage.vue'));
Vue.component('ExchangePage', () => import(/* webpackChunkName: "exchange-page" */ './components/exchange/ExchangePage.vue'));
Vue.component('SavingsAccountsPage', () => import(/* webpackChunkName: "savings-accounts-page" */ './components/savings-accounts/SavingsAccountsPage.vue'));
Vue.component('LoanPage', () => import(/* webpackChunkName: "loan-page" */ './components/loan/LoanPage.vue'));

new Vue({
  router,
  store,
  i18n,
  methods: {
    windowResize() {
      this.$store.commit('setWindowSize', window.innerWidth);
    },
    closeAllModal({ keyCode }) {
      if (keyCode === 27 && this.$store.state.activePanelPaymant !== 'successPayement') {
        this.$store.commit('closeAllModal');
        $('.not-index').removeClass('active');
      }
    },
    disabledLinkCurrPage(e) {
      if (e.target.tagName === 'A' && e.target.href.replace(e.data.origin, '') === e.data.pathname) e.preventDefault();
    },
  },
  mounted() {
    $(window).on('resize', this.windowResize);
    $(document).on('keydown', this.closeAllModal);
    const { pathname, origin } = window.location;
    $(document).on('click', { pathname, origin }, this.disabledLinkCurrPage);
  },
}).$mount('#app');

/* global ZohoHCAsap */
window.ZohoHCAsap = window.ZohoHCAsap || function (a, b) { ZohoHCAsap[a] = b; }; (function () { const d = document; const s = d.createElement('script'); s.type = 'text/javascript'; s.defer = true; s.src = 'https://desk.zoho.eu/portal/api/web/inapp/12095000000355001?orgId=20063777679'; d.getElementsByTagName('head')[0].appendChild(s); }());

const computedIsElemChild = (el, target) => (el.is(target) || el.has(target).length !== 0);
$(document).ready(() => {
  $(document).on('click', (e) => {
    const { target } = e;
    if (computedIsElemChild($('.jsGetChat'), target)) {
      $('#zohohc-asap-web-helper-main').addClass('active');
      ZohoHCAsap.Action('open');
    }
    if (computedIsElemChild($('#zohohc-asap-web-launcherbox-close'), target)) {
      $('#zohohc-asap-web-helper-main').removeClass('active');
    }
  });
  $('.load-page').hide();
});
