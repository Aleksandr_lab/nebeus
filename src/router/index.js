import Vue from 'vue';
import VueRouter from 'vue-router';
import SignUp from '../components/login/SignUp.vue';
import SignIn from '../components/login/SignIn.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/sign-up',
    name: 'SignUp',
    component: SignUp,
  },
  {
    path: '/sign-in',
    name: 'SignIn',
    component: SignIn,
  },
  {
    path: '/overwiev',
    name: 'Overwiev',
    component: () => import(/* webpackChunkName: "ContentOverwiev" */ '../components/crypto-currency/contentOverwiev/ContentOverwiev.vue'),
  },
  {
    path: '/history/:id?',
    name: 'History',
    component: () => import(/* webpackChunkName: "ContentHistory" */ '../components/crypto-currency/contentHistory/ContentHistory.vue'),
  },
  {
    path: '/cold/:coldId/:id?',
    name: 'ColdStorage',
    component: () => import(/* webpackChunkName: "ColdStorageContent" */ '../components/cold-storage/content/ColdStorageContent.vue'),
  },
  {
    path: '/exc/:exchangeId?',
    name: 'Exchange',
  },
  {
    path: '/save/history/',
    name: 'SaveHistory',
    component: () => import(/* webpackChunkName: "SaveHistory" */ '../components/savings-accounts/SaveContentHistory.vue'),
  },
  {
    path: '/save/overwiev/',
    name: 'SaveOverwiev',
    component: () => import(/* webpackChunkName: "SaveOverwiev" */ '../components/savings-accounts/SaveContentOverwiev.vue'),
  },
  {
    path: '/loan/history/',
    name: 'LoanHistory',
    component: () => import(/* webpackChunkName: "LoanHistory" */ '../components/loan/LoanHistory.vue'),
  },
  {
    path: '/loan/overwiev/',
    name: 'LoanOverwiev',
    component: () => import(/* webpackChunkName: "LoanOverwiev" */ '../components/loan/LoanOverwiev.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
