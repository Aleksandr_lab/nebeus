const wpPath = (path) => path.match(/\/wp-content\/.*/g)[0];

module.exports.wpPath = wpPath;
