module.exports = {
  filenameHashing: false,
  //productionSourceMap: false,
  publicPath: '/wp-content/themes/nebeus/resources/dist/',
  chainWebpack: (config) => {
    config.output.chunkFilename('js/chunk/[id].js');
  },
  css: {
    extract: true,
  },
  configureWebpack: {
    resolve: {
      alias: {
        vue$: 'vue/dist/vue.esm.js',
      },
    },
  },
};
